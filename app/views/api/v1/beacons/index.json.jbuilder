json.action "beacon_list"
json.response "true"
json.msg  "Successfully fetch beacons details"
json.is_active @user.is_active.to_s
json.access_permission @user.access_permission.to_s
json.results do
	if @beacon.size > 0
		json.array! @beacon.each do |res|
			json.name res.name
			json.door res.door.name
			json.ieee_id res.ieee_id
		end
	else
		json.array! @beacon
	end
end