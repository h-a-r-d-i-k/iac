class History < ActiveRecord::Base
  belongs_to :user_device
  belongs_to :user
  has_many :history_logs
end
