class Beacon < ActiveRecord::Base
	belongs_to :door
	validates_presence_of :name, :message => "can't be blank"
   validates_presence_of :ieee_id, :message => "can't be blank"
end
