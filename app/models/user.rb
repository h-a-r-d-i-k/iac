class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
          has_attached_file :photo, :styles => {:avatar => "100x100#"}

   has_many :user_devices
   has_many :histories
   # validates_confirmation_of :password
  #  validates_presence_of :first_name, :message => "can't be blank"
  #  validates_presence_of :last_name, :message => "can't be blank"
  # validates_presence_of :email, :message => "can't be blank"
  # validates_presence_of :password, :on => :create, :message => "can't be blank"
  # validates_presence_of :password_confirmation, :on => :create, :message => "can't be blank"
end
