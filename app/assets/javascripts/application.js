// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets

//= require jquery.min.js
//= require nprogress.js

//= require progressbar/bootstrap-progressbar.min.js
//= require icheck/icheck.min.js


//= require chartjs/chart.min.js


//= require flot/jquery.flot.js
//= require flot/jquery.flot.pie.js
//= require flot/jquery.flot.orderBars.js
//= require flot/jquery.flot.time.min.js
//= require flot/date.js
//= require flot/jquery.flot.spline.js
//= require flot/jquery.flot.stack.js
//= require flot/curvedLines.js
//= require flot/jquery.flot.resize.js

//= require flash.js


//= require moment/moment.min.js
