class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
 # skip_before_filter :verify_authenticity_token  
  before_action :authenticate_admin!
  skip_before_action :authenticate_user!
  # layout :determine_layout

  # def determine_layout
  #   module_name = self.class.to_s.split("::").first
  #   return (module_name.eql?("Devise") ? false : "application")
  # end
end
