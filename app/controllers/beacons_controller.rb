class BeaconsController < ApplicationController
  
def index
    @beacons = Beacon.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @beacon = Beacon.find(params[:id])
  end

  def new
     @beacon = Beacon.new
    # render layout: false
  end

  def edit
    @beacon = Beacon.find(params[:id])
  end

  def create
   @beacon = Beacon.create(beacon_params)
    redirect_to beacons_url  
  end

  def update
    @beacon = Beacon.find(params[:id])
    b =@beacon.update(beacon_params)
    redirect_to beacons_url 
  end

  def destroy
    @beacon = Beacon.find(params[:id])
    @beacon.destroy
    respond_to do |format|
      format.html { redirect_to beacons_url, notice: 'Beacon was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  
    def beacon_params
      params.require(:beacon).permit(:name,:door_id, :ieee_id)
    end
end
