# This controller is where clients can exchange
# codes and refresh_tokens for access_tokens

class Opro::Oauth::TokenController < OproController
  before_filter      :opro_authenticate_user!,    :except => [:create]
  skip_before_filter :verify_authenticity_token,  :only   => [:create]
  before_action :authenticate_admin!,    :except => [:create]

  def create
    # Find the client application
    @user = User.where(email: params[:email]).first
    
    if @user.blank?
      render :json => { action: "user_login",
                        # http://tools.ietf.org/html/rfc6749#section-5.1
                        response: "false",
                        message: "Wrong Email Id."}
    else
      valid = @user.valid_password?(params[:password])
      if valid == false
        render :json => { action: "user_login",
                        # http://tools.ietf.org/html/rfc6749#section-5.1
                        response: "false",
                        message: "Wrong email Password."}
      else
        if @user.is_active?
          if @user.access_permission?
            application = Opro::Oauth::ClientApp.where(user_id: @user.id).first
            u_device = UserDevice.where(device_token: params[:device_token]).first

            if u_device.blank?
              device = UserDevice.new
              device.user_id =  @user.id
              device.device_token = params[:device_token]
              device.device_type = params[:device_type]
              device.name = params[:device_name]
              device.save
              auth_grant = Opro::Oauth::AuthGrant.find_or_create_by_user_app(device, application)
            else
              token = Opro::Oauth::AuthGrant.where(user_device_id: u_device.id).first
              if !token.blank?
                token.delete
              end
              if u_device.user_id == @user.id
               auth_grant = Opro::Oauth::AuthGrant.find_or_create_by_user_app(u_device, application) 
              else
                device = UserDevice.new
                device.user_id =  @user.id
                device.device_token = params[:device_token]
                device.device_type = params[:device_type]
                device.name = params[:device_name]
                device.save
                auth_grant = Opro::Oauth::AuthGrant.find_or_create_by_user_app(device, application)
              end
            end
            
            # application = Opro::Oauth::ClientApp.authenticate(params[:client_id], params[:client_secret])
            # auth_grant  = auth_grant_for(application, params)
            @beacon = Beacon.all
            if auth_grant.present?
              auth_grant.refresh!
              render :json => {action: "user_login",
                              response: "true",
                              message: "User loged in successfully. ",
                              access_token:  auth_grant.access_token,
                              beacondetails: @beacon,
                              max_delay_to_pass_door_1: RssiTimer.last.timer_value,
                              max_delay_to_pass_door_2: RssiTimer.last.timer_value2,
                              thresold_value: RssiTimer.last.rssiLimit,
                              userdetails:
                               { 
                              user_id:   auth_grant.user_device.user_id,
                              first_name: auth_grant.user_device.user.first_name,
                              last_name: auth_grant.user_device.user.last_name,
                              mo_no: auth_grant.user_device.user.mo_no,
                              photo: auth_grant.user_device.user.photo.url,
                              email:     auth_grant.user_device.user.email,
                              user_device_id: auth_grant.user_device_id,
                              user_device_name:    auth_grant.user_device.name,
                              access_permission:  auth_grant.user_device.user.access_permission.to_s,
                              is_active: auth_grant.user_device.user.is_active.to_s
                              }}
            else
              render_error debug_msg(params, application)
            end
          else
          render :json => { action: "user_login",
                          # http://tools.ietf.org/html/rfc6749#section-5.1
                          response: "false",
                          message: "User has not Access permission. "}
          end
        else
          render :json => { action: "user_login",
                          # http://tools.ietf.org/html/rfc6749#section-5.1
                          response: "false",
                          message: "User Was not active. "}
        end
      end
    end
  end

  private

  def auth_grant_for(application, params)
    if params[:code]
      Opro::Oauth::AuthGrant.find_by_code_app(params[:code], application)
    elsif params[:refresh_token]
      Opro::Oauth::AuthGrant.find_by_refresh_app(params[:refresh_token], application)
    elsif params[:password].present? || params[:grant_type] == "password"|| params[:grant_type] == "bearer"
      return false unless Opro.password_exchange_enabled?
      return false unless oauth_valid_password_auth?(params[:client_id], params[:client_secret])
      user       = ::Opro.find_user_for_all_auths!(self, params)
      return false unless user.present?
      auth_grant = Opro::Oauth::AuthGrant.find_or_create_by_user_app(user, application)
      auth_grant.update_permissions if auth_grant.present?
      auth_grant
    end
  end

  def debug_msg(options, app)
    msg = "Could not find a user that belongs to this application"
    msg << " based on client_id=#{options[:client_id]} and client_secret=#{options[:client_secret]}" if app.blank?
    msg << " & has a refresh_token=#{options[:refresh_token]}" if options[:refresh_token]
    msg << " & has been granted a code=#{options[:code]}"      if options[:code]
    msg << " using username and password"                      if options[:password]
    msg
  end

  def render_error(msg)
    render :json => {:error => msg }, :status => :unauthorized
  end

end
