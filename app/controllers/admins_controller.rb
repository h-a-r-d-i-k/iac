class AdminsController < ApplicationController
  
  def edit
    @admin = Admin.find(params[:id])
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @admin = Admin.find(params[:id])
    u = @admin.update(admin_params)
    redirect_to root_url  
  end

  private

  def admin_params
    if params[:admin][:password].blank?
      params[:admin].delete(:password)
    end
    params.require(:admin).permit(:first_name,:last_name, :password, :email, :photo, :photo_file_name)
  end

end
