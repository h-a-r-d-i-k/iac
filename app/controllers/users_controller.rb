class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
  end

  # GET /users/new
  def new
    @user = User.new
    # render layout: false
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.create(user_params)
    if !@user.blank?
      Opro::Oauth::ClientApp.create_with_user_and_name(@user, @user.first_name+ @user.id.to_s + "IAC")
      redirect_to users_url 
      flash[:notice] = 'User created successfully!'
    else
      flash[:notice] = 'user was not added.'
      redirect_to users_url 
    end 
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    puts user_params_update
    u =@user.update(user_params_update)
    if !u.blank?
      flash[:notice] = 'User updated successfully!'
    else
      flash[:notice] = 'user was not updated.'
    end 
    redirect_to users_url  
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end
    def user_params_update
      if params[:user][:password].blank?
        params[:user].delete(:password)
      end
      params.require(:user).permit(:first_name,:last_name, :password, :email, :mo_no, :photo, :photo_file_name, :is_active,:access_permission)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:first_name,:last_name, :password, :email, :mo_no, :photo, :photo_file_name, :is_active,:access_permission)
    end
end
