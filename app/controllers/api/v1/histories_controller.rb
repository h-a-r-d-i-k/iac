module Api
	module V1 
		class HistoriesController < ApplicationController
			skip_before_action :authenticate_admin!
			skip_before_action :verify_authenticity_token
			def create
				history_temp = Array.new
				@user = User.find(params[:user_id])
				if params[:history] && params[:history] !=""
					history_params = ActiveSupport::JSON.decode(params[:history])
					puts history_params
					history_params['data'].each do |item|
						h = History.new
						h.entry_time	= item['entry_time']
						h.exit_time	= item['exit_time']
						h.user_device_id = item['user_device_id']
						h.user_id = params[:user_id]
						h.random_number = item['random_number']
						h.save
						history_temp << h
					end	
					render :json => { action: "history_add",
                        # http://tools.ietf.org/html/rfc6749#section-5.1
                        response: "true",
                        message: "history added successfully",
                        is_active: @user.is_active.to_s, 
                        access_permission:  @user.access_permission.to_s,
                        history_stored: history_temp}
		        else
		        	render :json => { action: "history_add",
                        # http://tools.ietf.org/html/rfc6749#section-5.1
                        response: "false",
                        message: "History was not added."}
				end
			end

			def log
				history_temp = Array.new
				@user = User.find(params[:user_id])
				if params[:history] && params[:history] !=""
					history_params = ActiveSupport::JSON.decode(params[:history])
					puts history_params
					history_params['data'].each do |item|
						# adding history parameter in history table
						if History.exists?(random_number: item['random_number'])
						  # your History id exists in the database
 							etime = item['exit_time']
						  	if (etime.present?)
						  		History.where(:random_number => item['random_number']).update_all(exit_time: etime)
						  	end
						  	# finding history id where history log need to be create
							# h  = History.where(["random_number = ?", item['random_number']]).take
							h = History.where(random_number: item['random_number']).take 
						else
						  # the History id doesn't exists
							h = History.new
							h.entry_time	= item['entry_time']
							h.exit_time	= item['exit_time']
							h.user_device_id = item['user_device_id']
							h.user_id = params[:user_id]
							h.random_number = item['random_number']
							h.save
						end
						# finding history id where history log need to be create
							h_id  = History.where(["random_number = ?", item['random_number']]).select('id').first
						# checking history log if exists removeing it
						if HistoryLog.exists?(history_id: h_id)
							HistoryLog.delete_all(history_id: h_id)
						end
						
						# creating history log
						item['historty_logs'].each do |l|
							h_log = HistoryLog.new
							h_log.history_id = h_id['id']
							h_log.random_number = l['random_number']
							h_log.log_time = l['log_time']
							h_log.log_type = l['log_type']
							h_log.log_description = l['log_description']
							h_log.save
						end
						history_temp << h
					end	
					render :json => { action: "history_add",
                        # http://tools.ietf.org/html/rfc6749#section-5.1
                        response: "true",
                        message: "history added successfully",
                        is_active: @user.is_active.to_s, 
                        access_permission:  @user.access_permission.to_s,
                        history_stored: history_temp}
		        else
		        	render :json => { action: "history_add",
                        # http://tools.ietf.org/html/rfc6749#section-5.1
                        response: "false",
                        message: "History was not added."}
				end
			end

	  	private
	  		def user_params
		    params.require(:history).permit(:entry_time,:exit_time, :user_device_id, :data[])
		   end
		end
	end
end
