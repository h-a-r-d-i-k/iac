class Api::V1::UsersController < ApplicationController
  skip_before_action :authenticate_admin!
  skip_before_action :verify_authenticity_token

  def logout
    if params[:access_token].blank?
      render :json => { action: "user_logout",
                        # http://tools.ietf.org/html/rfc6749#section-5.1
                        response: "false",
                        message: "provide access token"}
    else
      token = Opro::Oauth::AuthGrant.where(access_token: params[:access_token]).first
      if token.blank?
        render :json => { action: "user_logout",
                        # http://tools.ietf.org/html/rfc6749#section-5.1
                        response: "false",
                        message: "access token not available"}
      else
        # device = UserDevice.find(token.user_device_id)
        # device.delete
        token.delete
        render :json => { action: "user_logout",
                        # http://tools.ietf.org/html/rfc6749#section-5.1
                        response: "true",
                        message: "loged out successfully."}
      end
    end
  end

  def show
    if params[:id].blank?
      render :json => { action: "user_details",
                        # http://tools.ietf.org/html/rfc6749#section-5.1
                        response: "false",
                        message: "please user id"}
    else
      @user = User.find(params[:id])
       @beacon = Beacon.all
      if @user.blank?
        render :json => { action: "user_details",
                        # http://tools.ietf.org/html/rfc6749#section-5.1
                        response: "false",
                        message: "user not found"}
      else
       if @user.is_active.to_s == "false"
        render :json => { action: "user_details",
                        # http://tools.ietf.org/html/rfc6749#section-5.1
                        response: "false",
                        message: "user was not active",
                        is_active: @user.is_active.to_s}
       else
        render :json => { action: "user_details",
                        # http://tools.ietf.org/html/rfc6749#section-5.1
                        response: "true",
                        message: "user details get successfully.",
                        beacondetails: @beacon,
                        max_delay_to_pass_door_1: RssiTimer.last.timer_value,
                        max_delay_to_pass_door_2: RssiTimer.last.timer_value2,
                        thresold_value: RssiTimer.last.rssiLimit,
                        user:
                               { 
                              user_id:   @user.id,
                              first_name: @user.first_name,
                              last_name: @user.last_name,
                              mo_no: @user.mo_no,
                              photo: @user.photo.url,
                              email: @user.email,
                              access_permission:  @user.access_permission.to_s,
                              is_active: @user.is_active.to_s
                              }}
        end
      end
    end


  end
end
