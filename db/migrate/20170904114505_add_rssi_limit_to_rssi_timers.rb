class AddRssiLimitToRssiTimers < ActiveRecord::Migration
  def change
    add_column :rssi_timers, :rssiLimit, :integer
  end
end
