class CreateHistories < ActiveRecord::Migration
  def change
    create_table :histories do |t|
      t.references :user_device, index: true, foreign_key: true
      t.time :entry_time
      t.time :exit_time

      t.timestamps null: false
    end
  end
end
