class CreateBeacons < ActiveRecord::Migration
  def change
    create_table :beacons do |t|
      t.string :name
      t.integer :door_id
      t.string :ieee_id 
      t.timestamps null: false
    end
  end
end
