class CreateRssiTimers < ActiveRecord::Migration
  def change
    create_table :rssi_timers do |t|
      t.integer :timer_value

      t.timestamps null: false
    end
  end
end
