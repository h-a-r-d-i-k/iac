class CreateUserDevices < ActiveRecord::Migration
  def change
    create_table :user_devices do |t|
      t.integer :user_id
      t.string :name
      t.string :device_token
      t.string :device_type

      t.timestamps null: false
    end
  end
end
