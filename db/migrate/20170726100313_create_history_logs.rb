class CreateHistoryLogs < ActiveRecord::Migration
  def change
    create_table :history_logs do |t|
      t.references :history, index: true, foreign_key: true
      t.string :random_number
      t.datetime :log_time
      t.string :log_type
      t.string :log_description

      t.timestamps null: false
    end
  end
end
