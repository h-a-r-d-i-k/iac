class ChangeDataTypeForHistory < ActiveRecord::Migration
  def change
  	 change_column :histories, :entry_time, :datetime
  	 change_column :histories, :exit_time, :datetime
  end
end
